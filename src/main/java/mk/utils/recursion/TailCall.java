package mk.utils.recursion;

import java.util.function.Supplier;

public abstract class TailCall<T> {
    public abstract TailCall<T> resume();
    public abstract T eval();
    public abstract boolean isSuspend();

    public static <T> TailCall<T> res(T t) {
        return new Return<>(t);
    }

    public static <T> TailCall<T> sus(Supplier<TailCall<T>> s) {
        return new Suspend<>(s);
    }


    private static class Return<T> extends TailCall<T> {

        private final T result;

        private Return(T result) {
            this.result = result;
        }

        @Override
        public TailCall<T> resume() {
            throw new IllegalStateException("Call resume on Result");
        }

        @Override
        public T eval() {
            return result;
        }

        @Override
        public boolean isSuspend() {
            return false;
        }
    }

    private static class Suspend<T> extends TailCall<T> {

        private final Supplier<TailCall<T>> suspend;

        private Suspend(Supplier<TailCall<T>> suspend) {
            this.suspend = suspend;
        }

        @Override
        public TailCall<T> resume() {
            return suspend.get();
        }

        @Override
        public T eval() {
            TailCall<T> tmp = this;
            while (tmp.isSuspend()) {
                tmp = tmp.resume();
            }
            return tmp.eval();
        }

        @Override
        public boolean isSuspend() {
            return true;
        }
    }
}
