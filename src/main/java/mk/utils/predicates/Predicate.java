package mk.utils.predicates;

@FunctionalInterface
public interface Predicate<T> {
    boolean test(T t);
}
