package mk.utils.lists;

import mk.utils.functions.Function;
import mk.utils.predicates.Predicate;
import mk.utils.recursion.TailCall;

import static mk.utils.recursion.TailCall.res;
import static mk.utils.recursion.TailCall.sus;

public abstract class List<T> {
    public abstract T head();
    public abstract List<T> tail();
    public abstract boolean isEmpty();
    public List<T> push(T t) { return item(t, this); }
    public List<T> drop() {return tail();}
    public List<T> drop(long n) { return dropRec(n, this).eval(); }
    public List<T> drop(Predicate<T> predicate) {return  dropRec(predicate, this).eval(); }
    public <V> V fold(V identity, Function<V, Function<T,V>> f) {return foldRec(this, identity, f).eval();}
    public <V> V foldLeft(V identity, Function<V, Function<T,V>> f) {return fold(identity, f);}
    public <V> V foldRight(V identity, Function<V, Function<T,V>> f) {return revert().fold(identity, f);}
    public List<T> revert() {return foldLeft(list(), rs -> t -> item(t, rs));}
    public <V> List<V> map(Function<T,V> f) {return mapRec(this.revert(), list(), f).eval();}

    private List() {}

    private static <T> List<T> item(T head, List<T> tail) {
        return new Item<>(head, tail);
    }

    @SuppressWarnings("rawtypes")
    public static final List nil = new Nil();

    @SuppressWarnings("unchecked")
    public static <T> List<T> list() {
        return nil;
    }

    @SafeVarargs
    public static <T> List<T> list(T ... ts) {
        List<T> result = list();
        for (int i = ts.length - 1; i >= 0; --i) {
            result = item(ts[i], result);
        }
        return result;
    }


    @Override
    public String toString() {
        return "[" + toStringRec(new StringBuilder(), this).eval().toString() + "Nil]";
    }

    private static class Nil<T> extends List<T> {
        private Nil() {
        }

        @Override
        public T head() {
            throw new IllegalStateException("Call head on NIL list item");
        }

        @Override
        public List<T> tail() {
            throw new IllegalStateException("Call tail on NIL list item");
        }

        @Override
        public boolean isEmpty() {
            return true;
        }

        @Override
        public String toString() {
            return "[Nil]";
        }
    }

    private static class Item<T> extends List<T> {
        private final T head;
        private final List<T> tail;

        private Item(T head, List<T> tail) {
            this.head = head;
            this.tail = tail;
        }

        @Override
        public T head() {
            return head;
        }

        @Override
        public List<T> tail() {
            return tail;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }
    }

    private static <T> TailCall<StringBuilder> toStringRec(StringBuilder acc, List<T> list) {
        return list.isEmpty() ?
                res(acc) :
                sus(() -> toStringRec(acc.append(list.head().toString()).append("->"), list.tail()));
    }

    private static <T> TailCall<List<T>> dropRec(long n, List<T> list) {
        return n == 0 ?
                res(list) :
                sus(() -> dropRec(n-1, list.tail()));
    }

    private static <T> TailCall<List<T>> dropRec(Predicate<T> predicate, List<T> list) {
        return list.isEmpty() || !predicate.test(list.head())?
                res(list) :
                sus(() -> dropRec(predicate, list.tail()));
    }

    private static <T,V> TailCall<V> foldRec(List<T> list, V acc, Function<V, Function<T,V>> f) {
        return list.isEmpty() ?
                res(acc) :
                sus(() -> foldRec(list.tail(), f.apply(acc).apply(list.head()), f));
    }

    private static <T,V> TailCall<List<V>> mapRec(List<T> list, List<V> acc, Function<T,V> f) {
        return list.isEmpty() ?
                res(acc) :
                sus(() -> mapRec(list.tail(), item(f.apply(list.head()), acc), f));
    }
}
