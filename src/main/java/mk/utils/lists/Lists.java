package mk.utils.lists;

import mk.utils.functions.Function;
import mk.utils.recursion.TailCall;

import java.util.*;
import java.util.List;

import static mk.utils.recursion.TailCall.res;
import static mk.utils.recursion.TailCall.sus;

public class Lists {

    public static <T> List<T> list() {
        return Collections.emptyList();
    }

    public static <T> List<T> list(T t) {
        return Collections.singletonList(t);
    }

    public static <T> List<T> list(List<T> ts) {
        return Collections.unmodifiableList(new ArrayList<>(ts));
    }

    @SafeVarargs
    public static <T> List<T> list(T ... ts) {
        return Collections.unmodifiableList(Arrays.asList(Arrays.copyOf(ts, ts.length)));
    }

    public static <T> T head(List<T> list) {
        return Optional.ofNullable(list)
                .map(l -> l.get(0))
                .orElseThrow(() -> new IllegalStateException("Head called on empty list"));
    }

    public static <T> List<T> tail(List<T> ts) {
        return Optional.ofNullable(ts)
                .map(Lists::copy)
                .map(l -> {l.remove(0); return l;})
                .orElseThrow(() -> new IllegalStateException("Tail called on empty list"));
    }

    public static <T> List<T> append(List<T> ts, T t) {
        return Optional.ofNullable(ts)
                .map(Lists::copy)
                .map(l -> {l.add(t); return l;})
                .map(Lists::list)
                .orElseThrow(() -> new IllegalStateException("Append to list exception"));
    }


    // TODO: improve efficiency
    public static <T> List<T> prepend(T t, List<T> ts) {
        return foldLeft(ts, list(t), acc -> tx -> append(acc, tx));
    }

    public static <T,V> V fold(List<T> ts, V identity, Function<V, Function<T,V>> f) {
        return foldRec(ts, identity, f).eval();
    }

    public static <T,V> V foldLeft(List<T> ts, V identity, Function<V, Function<T,V>> f) {
        return fold(ts, identity, f);
    }

    public static <T,V> V foldRight(List<T> ts, V identity, Function<V, Function<T,V>> f) {
        return foldRec(reverse(ts), identity, f).eval();
    }

    public static <T> List<T> reverse(List<T> ts) {
        // return foldLeft(ts, list(), acc -> t -> prepend(t, acc));
        List<T> result = new ArrayList<>();
        for (int i = ts.size() - 1; i >= 0; --i) {
            result.add(ts.get(i));
        }
        return Collections.unmodifiableList(result);
    }

    public static <T,V> List<V> map(List<T> ts, Function<T, V> f) {
        return foldLeft(ts, Lists.list(), vs -> t -> append(vs, f.apply(t)));
    }

    private static <T> List<T> copy(List<T> ts) {
        return new ArrayList<>(ts);
    }

    private static <T,V> TailCall<V> foldRec(List<T> ts, V acc, Function<V, Function<T,V>> f) {
        return ts.isEmpty() ?
                res(acc) :
                sus(() -> foldRec(tail(ts), f.apply(acc).apply(head(ts)), f));
    }
}
