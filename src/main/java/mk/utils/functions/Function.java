package mk.utils.functions;

@FunctionalInterface
public interface Function<X, Y> {
    Y apply(X x);

    default <Z> Function<Z,Y> compose(Function<Z,X> f) {
        return z -> this.apply(f.apply(z));
    }

    default <Z> Function<X,Z> andThen(Function<Y,Z> f) {
        return x -> f.apply(this.apply(x));
    }

    static <T> Function<T,T> identity() {
        return t -> t;
    }

    static <X,Y,Z> Function<X,Z> compose(Function<Y,Z> f, Function<X,Y> g) {
        return x -> f.apply(g.apply(x));
    }

    static <X,Y,Z> Function<X,Z> andThen(Function<X,Y> f, Function<Y,Z> g) {
        return x -> g.apply(f.apply(x));
    }

    static <X,Y,Z> Function<Function<X,Y>, Function<Function<Y,Z>, Function<X,Z>>> andThen() {
        return x -> y -> xs -> y.apply(x.apply(xs));
    }

    static <X,Y,Z> Function<Function<Y,Z>, Function<Function<X,Y>, Function<X,Z>>> compose() {
        return y -> x -> xs -> y.apply(x.apply(xs));
    }
}
