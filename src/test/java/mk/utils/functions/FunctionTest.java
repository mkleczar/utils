package mk.utils.functions;


import mk.utils.functions.Function;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FunctionTest {

    @Test
    public void testHighOrderComposeAndThen() {
        Function<Integer, Integer> twice = (Integer i) -> 2 * i;
        Function<Integer, Integer> inc = (Integer i) -> i + 1;

        int result = Function.<Integer, Integer, Integer>compose().apply(twice).apply(inc).apply(4);
        assertEquals("(4+1)*2=10", 10, result);

        result = Function.<Integer, Integer, Integer>andThen().apply(twice).apply(inc).apply(4);
        assertEquals("4*2+1=9", 9, result);

    }
}
