package mk.utils.lists;

import org.junit.Test;

import static mk.utils.lists.List.list;
import static mk.utils.lists.List.nil;
import static org.junit.Assert.*;

public class ListTest {

    @Test
    public void testEmptyListCreation() {
        List<String> list = list();
        assertTrue("List created by list() should be empty", list.isEmpty());
        assertEquals("List created by list() is just a nil", list, nil);
    }

    @Test
    public void testNotEmptyListCreation() {
        List<String> list = list("a", "b");
        assertFalse("List created by list(some args) should not be empty", list.isEmpty());
    }

    @Test
    public void testHeadAndTail() {
        List<String> list = list("a", "b");
        assertEquals("Head of <'a', 'b'> list is 'a'", "a", list.head());
        assertEquals("Head of  tail of <'a', 'b'> list is 'b'", "b", list.tail().head());
        assertEquals("Head of  tail of tail of <'a', 'b'> list is nil", nil, list.tail().tail());
    }

    @Test
    public void testPush() {
        List<String> list = list();
        list = list.push("a").push("b");
        assertEquals("Head of <'b', 'a'> list is 'b'", "b", list.head());
        assertEquals("Head of  tail of <'b', 'a'> list is 'a'", "a", list.tail().head());
    }

    @Test
    public void toStringTest() {
        assertEquals("toString for empty list", "[Nil]", list().toString());
        assertEquals("toString for non empty list", "[a->b->Nil]", list("a", "b").toString());
    }

    @Test
    public void dropTest() {
        List<Integer> list = list(1,2,3,4);
        List<Integer> result = list.drop();
        assertEquals("list after one drop", "[2->3->4->Nil]", result.toString());
        result = result.drop(2);
        assertEquals("list after three drop", "[4->Nil]", result.toString());
    }

    @Test
    public void dropToEmptyTest() {
        List<Integer> list = list(1,2,3,4);
        List<Integer> result = list.drop(4);
        assertTrue("list should be empty", result.isEmpty());
    }

    @Test(expected = IllegalStateException.class)
    public void dropOnEmptyTest() {
        list(1,2,3,4).drop(5);
    }

    @Test
    public void dropPredicateTest() {
        List<String> list = list("", "", "", "a", "");
        List<String> result = list.drop(String::isEmpty);
        assertEquals("drop empty strings only from begining", "a", result.head());
        assertEquals("drop empty strings only from begining", "", result.tail().head());
    }

    @Test
    public void foldLeftTest() {
        List<Integer> list = list(1,2,3,4);
        int sumOfSquare = list.foldLeft(0, acc -> i -> acc + i*i);
        assertEquals("1^2 + 2^2 + 3^2 + 4^2 = 30", 30, sumOfSquare);
    }

    @Test
    public void foldRightAndLeftTest() {
        List<String> list = list("a","b","c");
        String result = list.foldRight("", acc -> s -> "("+s+acc+")");
        assertEquals("fold right with asimetric operator", "(a(b(c)))", result);
        result = list.foldLeft("", acc -> s -> "("+s+acc+")");
        assertEquals("fold left with asymetric operator", "(c(b(a)))", result);
    }

    @Test
    public void revertEmptyTest() {
        assertEquals("reverse of Nil is Nil", list(), list().revert());
    }


    @Test
    public void revertNotEmptyTest() {
        String str = list(1,2,3,4).revert().toString();
        assertEquals("[4->3->2->1->Nil]", str);
    }


    @Test
    public void mapTest() {
        List<Integer> list = list(1,2,3,4);
        Integer result = list
                .map(i -> i*i)
                .fold(0, acc -> i -> acc+i);
        assertEquals("1^2 + 2^2 + 3^2 + 4^2 = 30", 30, result.intValue());
    }

    @Test
    public void mapStringTest() {
        List<Integer> list = list(1,2,3,4);
        String result = list
                .map(i -> ""+i)
                .fold("", acc -> i -> acc + i);
        assertEquals("map integer and catenation", "1234", result);
    }

}
