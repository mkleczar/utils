package mk.utils.lists;

import mk.utils.functions.Function;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static mk.utils.lists.Lists.*;
import static org.junit.Assert.assertEquals;

public class ListsTest {

    private static final Logger logger = LoggerFactory.getLogger(ListsTest.class);

    @Test
    public void testFoldSumArray() {
        List<Integer> is = list(0,1,2,3,4,5,6,7,8,9);
        Integer result = fold(is, 0, x -> y -> x+y);
        assertEquals("Sum from 0 to 10 is 45", (Integer) 45, result);
    }

    @Test
    public void testFoldJoinString() {
        List<String> ss = list("a", "b", "c", "d");
        String result = fold(tail(ss), head(ss), sa -> s -> sa + ", " + s);
        assertEquals("joining a, b, c, d", "a, b, c, d", result);
    }

    @Test
    public void foldLeftVsRight() {
        List<String> ss = list("a", "b", "c", "d");
        String resultFoldLeft = foldLeft(ss, "", sa -> s -> "(" + sa + ", " + s + ")");
        String resultFoldRight = foldRight(ss, "", sa -> s -> "(" + s + ", " + sa + ")");

        assertEquals("((((, a), b), c), d)", resultFoldLeft);
        assertEquals("(a, (b, (c, (d, ))))", resultFoldRight);
        //logger.info("foldLeft: {}", resultFoldLeft);
        //logger.info("foldRight: {}", resultFoldRight);
    }

    @Test
    public void revertTest() {
        List<Integer> ls = list(1,2,3,4);
        List<Integer> reverted = reverse(ls);
        assertEquals("reverted list expected", (List<Integer>)list(4,3,2,1), reverted);
    }

    @Test
    public void mapTest() {
        List<Integer> ls = list(1,2,3,4);
        Function<Integer, String> mapper = i -> "" + i;
        List<String> result = map(ls, mapper);
        assertEquals(list("1", "2", "3", "4"), result);
    }
}
