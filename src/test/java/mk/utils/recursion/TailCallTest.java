package mk.utils.recursion;

import org.junit.Test;

import static mk.utils.recursion.TailCall.res;
import static mk.utils.recursion.TailCall.sus;
import static org.junit.Assert.assertEquals;

public class TailCallTest {


    private static int add(int x, int y) {
        return addRec(x, y).eval();
    }

    private static TailCall<Integer> addRec(int x, int y) {
        return  (x == 0) ?
            res(y) :
            sus(() -> addRec(x -1, y + 1));
    }

    private static int addClassicRec(int x, int y) {
        return (x == 0) ?
                y:
                addClassicRec(x-1, y+1);
    }

    @Test
    public void testAddTailCall() {
        assertEquals("2+3=5", 5, add(2,3));
        assertEquals("20+30=50", 50, add(20,30));
        assertEquals("200+300=500", 500, add(200,300));
        assertEquals("2000+3000=5000", 5000, add(2000,3000));
        assertEquals("20000+30000=50000", 50000, add(20000,30000));
        assertEquals("200000+300000=500000", 500000, add(200000,300000));
        assertEquals("2000000+3000000=5000000", 5000000, add(2000000,3000000));
        assertEquals("20000000+30000000=50000000", 50000000, add(20000000,30000000));
    }

    @Test(expected = StackOverflowError.class)
    public void testAddClassicRes() {
        assertEquals("20000000+30000000=50000000", 50000000, addClassicRec(20000000,30000000));
    }
}
